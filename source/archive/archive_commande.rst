.. _archive_commande:

######################
archive des commandes
######################



Il est possible de visualiser les commandes archivées dans le menu archive

.. image:: img/tab_archive_commande.png

Il est possible de les supprimer

.. image:: img/form_archive_commande.png

L'édition de la commande et des livraisons associées sont visible dans l'onglet dossier :

.. image:: img/tab_dossier_commande.png

Il est possible de le supprimer

.. image:: img/form_dossier_commande.png

Attention le dossier et archive commande sont indépendants (pas d'intégrité avec une clé secondaire)
