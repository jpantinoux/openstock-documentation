.. _archive_entre:

###################
archive des entrees
###################



Il est possible de visualiser les entrées archivées dans le menu archive

.. image:: img/tab_archive_entre.png

Il est possible de les supprimer

.. image:: img/form_archive_entre.png
