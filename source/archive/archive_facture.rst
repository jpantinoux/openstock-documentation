.. _archive_facture:

######################
archive des factures
######################



Il est possible de visualiser les factures archivées dans le menu archive

.. image:: img/tab_archive_facture.png

Il est possible de les supprimer

.. image:: img/form_archive_facture.png

L'édition de la facture  associée est visible dans l'onglet dossier :

.. image:: img/tab_dossier_facture.png

Il est possible de le supprimer

.. image:: img/form_dossier_facture.png

Attention le dossier et archive facture sont indépendants (pas d'intégrité avec une clé secondaire)
