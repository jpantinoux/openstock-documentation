.. _archive_livraison:

######################
archive des livraisons
######################



Il est possible de visualiser les livraisons archivées dans le menu archive

.. image:: img/tab_archive_livraison.png

Il est possible de les supprimer

.. image:: img/form_archive_livraison.png

L'édition de la livraison est visible dans l'onglet dossier :

.. image:: img/tab_dossier_livraison.png

Il est possible de le supprimer

.. image:: img/form_dossier_livraison.png

Attention le dossier et archive livraison sont indépendants (pas d'intégrité avec une clé secondaire)
