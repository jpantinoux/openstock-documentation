.. _version_3.0.0:

###############################
Migration vers la version 3.0.0
###############################


Nous allons traiter dans ce chapitre les problèmes de migration de la version 2.0.2
vers la version 3.0.0

Il est rappeler les différences de version ::

    openstock 2.02 est sur une version du framework openMairie 2.x
    openstock 2.02 utilise mysql, la version 3.0.0 postgres
    Il n'existe plus dans la version 3.0.0 que l option form : recgroupement et facture
    
    
les différences postgres / mysql ::

    date par defaut 0000/00/00 non acceptés dans postgres
    intégrité relationnelle des clés secondaires (0 comme clé numérique non acceptée)
    il est utilisé les champs boolean de postgres (true/false au lieu de Oui/non en varchar3)
    
nous proposons ci dessous nos requêtes qui pourront être reprises ou corrigées pour vos bases
de données spécifiques.

Nous ne proposons pas de traitement automatique.


==================================
Transfert application regroupement
==================================






    





