.. _version_4.0.0:

###############################
Migration vers la version 4.0.0
###############################


Nous allons traiter dans ce chapitre les problèmes de migration de la version 3.0.0
vers la version 4.0.0

Dans le fichier data/ver_4.0.0.sql, il est noté les différences dans la base de données

- normalisation sortie article et sortie prestation

- ajout des tables unité et tva (en remplacement de var.inc)

Il a été repris entièrement le module d'archivage.

Il a été mis en place des triggers et procédures stockées pour les calculs de stocks et des vues dans postgres pour le cumul

La version 4.0.0 est une mise à niveau avec openMairie 4.9.2


Plusieurs fonctionnalités ont été rajoutées:

- planning, ressource et catégorie de ressource permettent la planification des bons de commandes (ou factures en cours)

- géolocalisation des clients et module géographique






    





