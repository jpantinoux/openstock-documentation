.. _article:

=======
Article
=======

Il s'agit des articles du stock

L'accès en saisie se fait par le menu paramétrage -> article :

.. image:: img/tab_article.png

En saisie

.. image:: img/form_article.png


L'onglet entree permet d'accéder aux entrées de l'article correspondant

L'onglet sortie permet d'accéder aux sorties de l'article correspondant

L'onglet entre_archive permet d'accéder aux entrées de l'article archivées

L'onglet sortie_archive permet d'accéder aux sorties de l'article archivées

Il est noté dans le formulaire les commandes d'article non livrées.
