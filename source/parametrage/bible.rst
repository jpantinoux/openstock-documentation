.. _bible:

=====
Bible
=====

La bible permet d'alimenter les champs observations dans le devis, contrat, bon de commande et
condition de facturation

L'accès en saisie se fait par le menu paramétrage -> bible :

.. image:: img/tab_bible.png

En saisie

.. image:: img/form_bible.png

nom_table est la table correspondante

nom_champ est le champ correspondant

ordre = ordre d'affichage dans le remplissage de l'aide à la saisie

automatique = remplissage automatique du champ

Possibilités ::

    devis / observation
    facture/observation (bon de commande)
    condition_facture/observation


