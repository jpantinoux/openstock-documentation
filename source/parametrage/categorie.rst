.. _categorie:

=========
Categorie
=========

Il s'agit de la categorie d'un acteur

L'accès en saisie se fait par le menu paramétrage -> categorie :

.. image:: img/tab_categorie.png

En saisie 

.. image:: img/form_categorie.png

L'onglet prestation permet d'accéder aux prestations de la catégorie

