.. _categorie_ressource:

======================
Categorie de ressource
======================

Il s'agit de la categorie de rssource

L'accès en saisie se fait par le menu paramétrage -> categorie_ressource :

.. image:: img/tab_categorie_ressource.png

En saisie 

.. image:: img/form_categorie_ressource.png

L'onglet ressource permet d'accéder aux ressources de la catégorie

