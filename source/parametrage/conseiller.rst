.. _conseiller:

==========
Conseiller
==========

Il s'agit du conseiller dans le cadre de l'établissement d'un devis ou d'un bon de commande (champ non obligatoire)

L'accès en saisie se fait par le menu paramétrage :

.. image:: img/tab_conseiller.png

En saisie

.. image:: img/form_conseiller.png

Il faut saisir

- le libelle
