.. _om_parametre:

============
om_parametre
============


Les om_parametres sont accessibles dans le menu administration -> om_parametre




Ils sont initialisés pour chaque organisme / collectivité.


Les paramètres suivants sont à initialiser

Paramètres généraux
===================

- organisme : nom de l'organisme  dans le menu action (haut droit)

- cp

- ville

- numero facture = -R  entraine un numéro facture de la forme 2014-R000001

- numérodevis = -Y entraine un numero devis de la forme 2014-Y000001

- delai reglement = 2  (2 mois)

- lon : longitude pour la recherche d'adresse dans la BAN (base d'adresse nationale)

- lat : latitude pour lea recherche d'adresse dans la BAN (base d'adresse nationale)

- option_localisation : sig_interne (utilisation du sig interne openmairie)

- option_facture : true permet l accès au menu facture + régie + versement


om_parametre utilisés dans les états
====================================

- complement

- emetteur

- adresse_emetteur

- cp_emetteur

- ville_emetteur

- type_contrat

- structure

- site



