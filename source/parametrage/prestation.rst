.. _prestation:

==========
Prestation
==========

Il s'agit des prestations

L'accès en saisie se fait par le menu paramétrage -> prestation :

.. image:: img/tab_prestation.png

En saisie

.. image:: img/form_prestation.png


La catégorie est au choix


L'onglet sortie permet d'accéder aux sorties de la prestation correspondante


L'onglet sortie_archive permet d'accéder aux sorties de prestations archivées
