.. _unite:

=====
Unite
=====

Il s'agit de l'unité utilisé en prestation (jour, heure, forfait ...)

L'accès en saisie se fait par le menu paramétrage -> categorie :

.. image:: img/tab_unite.png

En saisie 

.. image:: img/form_unite.png

L'onglet prestation permet d'accéder aux prestations correspondantes
