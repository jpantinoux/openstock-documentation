.. _planning:


La planification des bons de commande sont accessibles dans le menu -> planning

Il s'agit d'affecter une ressource à un bon de commande au travers
d'un planning


.. image::menu_planning.png

.. toctree::

    ressource.rst
    planning.rst
