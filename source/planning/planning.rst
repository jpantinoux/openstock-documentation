.. _planning:

########
Planning
########

Il s'agit d'affecter une ressource à un bon de commande au travers
d'un planning


Il est possible d'accéder au planning dans le menu planning ou dans le sous 
formulaire de bon de commande ou le sous formulaire de ressource

.. image:: img/tab_planning.png

Il est possible de les supprimer

.. image:: img/form_planning.png

Onglet :

- facture en cours ou bon de commande

- ressource
