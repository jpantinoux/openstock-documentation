.. _ressource:

#########
Ressource
#########


Il est possible d'accéder aux ressources dans le menu planning

.. image:: img/tab_ressource.png

Il est possible de les supprimer

.. image:: img/form_ressource_planning.png

Les champs couleur fond et police ne sont pas encore utilisés pour le planning.


Il est possible de rajouter un planning dans l'affichage de la ressource en choisissant un bon de commande
et en cliquant sur le planning au jour et heure correspondant.

.. image:: img/planning_ajout.png

Il est possible de détruire un créneau de planning

.. image:: img/planning_destruction.png

Il est possible de réduire, agrandir ou déplacer un créneau du planning.

L'affichage du planning se fait par semaine, jour ou mois suivant les options proposées

.. image:: img/option_planning.png
