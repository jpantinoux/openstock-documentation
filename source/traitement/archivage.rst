.. _archivage:

#########
archivage
#########

L'accès se fait par le menu traitement -> archivage

L'archivage global n'est pas implémenté dans cette version.

Les archives sont faites dans les actions des formulaires :
- livraison
- commande
- facture
- régie
- regroupement

