.. _registre:

########
Registre
########


L'accès se fait par le menu traitement -> registre facture / registre devis

Le traitement permet de remettre à O le registre des factures ou le registre des devis

Ce traitement est à faire en début d'année.

.. image:: misea0registredevis.png

.. image:: misea0registrefacture.png





