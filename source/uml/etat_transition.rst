.. _etat_transition:

######################################
Diagramme état transition des factures
######################################


facture
=======

Le diagramme état transition permet de décrire :

- les différents états de la facture

- les transsitions pour passer d'un état à l'autre.

.. image:: etat_transition_facture.png

