.. _devis:

#####
Devis
#####

Le devis est utile en gestion commerciale.

Le devis n'impacte pas les sorties d'article dans le stock.

Il est possible ensuite de transferer le devis en bon de commande. A ce moment là, la sortie d'article est impactée.


Saisie des devis
================

Il est possible de modifier les devis dans le menu sortie

.. image:: img/tab_devis.png

En appuyant sur modification :

.. image:: img/form_devis.png

Attention, la suppression du devis suprime toutes les lignes du devis (articles et prestations)

Il est possible de trouver un client avec la recherche 
(il faut saisir au moins 2 caractères).

.. image:: img/search_client.png


Saisie des sorties articles
===========================

Il est possible d'ajouter  les sorties articles en appuyant sur le + :

La recherche d'article se fait en tapant les premiers caractères dans le champ recherche devis :

.. image:: img/search_article.png 

Appuyer ensuite sur ajouter

.. image:: img/form_lignedevis_article.png

Le message suisvant apparait :

.. image:: img/form_lignedevis_article_retour.png

Pour supprimer un article appuyer sur "X" correspondant :

.. image:: img/form_lignedevis_article_sup.png
    




Saisie des sorties prestations
==============================

Il est possible d'ajouter et de supprimer les sorties de prestations 

La recherche d'article se fait en tapant les premiers caractères dans le champ recherche devis :

.. image:: img/search_prestation.png 

Appuyer ensuite sur ajouter

.. image:: img/form_lignedevis_prestation.png

Le message suisvant apparait :

.. image:: img/form_lignedevis_prestation_retour.png

Pour supprimer un article appuyer sur "X" correspondant :

.. image:: img/form_lignedevis_prestation_sup.png


les actions :
=============
    
les actions suivantes sont possibles : 

.. image:: img/action_devis.png


Action Saisie de l'observation
==============================

Il est possible de modifier de l'observation qui se situe en entête du devis dans l'action correspondante

.. image:: img/form_observation.png


Transfert du devis
==================

Le devis est transféré en bon de commande en appuyant sur l'action transfert en bon de commande

Il est demandé confirmation :

.. image:: img/confirmation.png


Attention, il n est pas possible de transférer un devis si le stock n'est pas approvisionné
(stock = 0 ou négatif)

.. image:: img/action_devis_transfert.png

Edition simple du devis 
=======================
Il est possible d'éditer le devis avec l'action édition simple ou édition par catégorie :

.. image:: img/action_devis_edition_simple.png

