.. _facture_edition:

##################
Facture en édition
##################


Une facture éditée est donnée au client.

Saisie des factures en édition
==============================

Les actions suivantes sont possibles :

.. image:: img/action_facture_edition.png

- l'état de la facture : retour état en cours ou reglement

- type de versement


Il n'est plus possible de modifier le montant de la facture. 
Pour modifier, il faut revenir en état "encours"
c'est à dire en bon de commande

Une facture EN EDITION ne peut pas être supprimée.

ATTENTION :

Lorsque la facture est en édition,
il n'est plus possible de modifier les entrés / sorties de stocks.

Les versements
===============

Il est possible de saisir  les versements en onglet :

.. image:: img/tab_versement.png

En appuyant sur modification :

.. image:: img/form_versement.png

Il est possible de supprimer un versement et le message suivant apparait :

.. image:: img/form_versement_sup.png


Les actions:
============

Edition des imputations de la facture

Cette édition détaille l'imputation comptable des éléments de la facture (utile en comptabilité)

.. image:: img/action_edition_imputation.png


Edition de la lettre d'accompagnement de la facture

.. image:: img/action_edition_lettre.png

Edition du rappel de la facture

.. image:: img/action_edition_rappel.png
