.. _facture_encours:

###################################
Bon de commande ou facture en cours
###################################

Le bon de commande correspond à l'état en cours de la facture.

Saisie des bons de commande
===========================

Un bon de commande peut être créé par un transfert de devis ou par une saisie
directe.

Il est possible de modifier les bons de commandes dans le menu sortie

.. image:: img/tab_facture_encours.png

En appuyant sur modification :

.. image:: img/form_facture_encours.png

Il est possible de trouver un client avec la recherche 
(il faut saisir au moins 2 caractères).

.. image:: img/search_client.png


Les actions :
=============

Les actions suivantes sont possibles :

.. image:: img/action_facture_encours.png

Saisie des conditions de facturation
====================================

Il est possible de modifier les conditions de facturation dans l'action correspondante

.. image:: img/form_facture_condition_facture.png

Saisie de l'entete (observation)
================================

Il est possible de modifier l'entête de la facturation dans l'action correspondante

.. image:: img/form_observation.png

Saisie des sorties articles
===========================

Il est possible d'ajouter des sorties articles dans la liste correspondante

Attention le bon de commande impacte la sortie d'article dans le stock


En appuyant sur + :

.. image:: img/form_sortie_article.png

Il est possible de trouver un article  avec la recherche 
(il faut saisir au moins 2 caractères).

Il n'est plus possible de sortir une quantité supérieure au stock

.. image:: img/form_sortie_article_quantite_insuffisante.png

Saisie des sorties prestations
==============================

Il est possible d'ajouter et supprimer les sorties de prestations dans la liste correspondante

En appuyant sur + :

.. image:: img/form_sortie_prestation.png

Il est possible de trouver une prestation avec la recherche 
(il faut saisir au moins 2 caractères).




Les éditions
============

Bon de sortie d'article

.. image:: img/action_facture_edition_prebon.png

Bon de commande

.. image:: img/action_facture_encours_simple.png


