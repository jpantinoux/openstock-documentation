.. _fournisseur:

###########
fournisseur
###########

Saisie des fournisseurs
=======================

Il est possible de modifier les fournisseurs dans le menu entree

.. image:: img/tab_fournisseur.png

En appuyant sur modification :

.. image:: img/form_fournisseur.png

L'utilisateur doit saisir :

- le libelle

- l'adresse 

En tapant le début de l'adresse, l'adresse est rechérchée dans la base
d'adresse nationale (BAN) et elle est récupérée ainsi que le code postal et
la ville correspondante.

Lea recherche par le CP renvoie la ville et vice versa.

Il est rapatrié la géolocalisation de l'adresse (coordonnées géographique) et 
il est possible de visualiser l'adresse du fournisseur sur la carte en appuyant sur le bouton suivant :

.. image:: img/geom.png

On obtient la carte suivante :

.. image:: img/sig_fournisseur.png


Catalogue 
=========

Sur la base des articles en stock, il est possible de saisir le catalogue du fournisseur
avec le prix d'achat et la quantité minimum d'achat dans le sous formulaire catalogue.

.. image:: img/tab_catalogue.png

En appuyant sur modification :

.. image:: img/form_catalogue.png




Commandes
=========

Il est possible de consulter les commandes non archivées dans le sous formulaire commande


Livraisons
==========

Il est possible de consulter les livraisons non archivées dans le sous formulaire livraison


Dossier
=======

Il est possible de consulter les livraisons et les commandes non archivées dans le sous formulaire dossier :

.. image:: img/tab_dossier_fournisseur.png

et les modifier ;

.. image:: img/form_dossier_fournisseur.png

