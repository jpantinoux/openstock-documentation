.. _utilisation:



Il est proposer de décrire l'utilisation de l'application dans ce chapitre pour la gestion commerciale et le regroupement

Il y a deux manières d'utiliser openStock pour les sorties d'article et prestation:

- en gestion commerciale avec une facturation externe à des clients avec un encaissement en régiec

- en affectant les sorties à des regroupements (service, écoles, véhicules, batiments ...)

Ci dessous le menu sortie :

.. image:: img/menu_sortie.png

Dans tous les cas les entrées d'articles sont communs : commande / livraison / client

Ci dessous le menu entre

.. image:: img/menu_entre.png


.. toctree::

    commande.rst
    livraison.rst
    fournisseur.rst 
    client.rst
    devis.rst
    facture_encours.rst
    facture_edition.rst
    reglement.rst
    facture_solde.rst
    regie.rst
    regroupement.rst
    