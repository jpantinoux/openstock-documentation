.. _livraison:

#########
Livraison
#########

Saisie des livraisons
=====================

Il est possible de modifier les livraisons dans le menu entree -> livraison ou dans le
sous formulaire "bon de livraison" du formulaire de commande. 

.. image:: img/tab_livraison.png

En appuyant sur modification :

.. image:: img/form_livraison.png

Le solde des livraisons permet de pouvoir lancer la procédure d'archivage des commandes.



