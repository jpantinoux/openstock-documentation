.. _regie:

#####
Regie
#####

La régie n'est utile qu en gestion commerciale.

La régie est un versement groupé des paiements de factures à la banque (entreprise)
ou au receveur (organisme public). C'est un moyen de solder les factures.


Saisie des regies
==================

Il est possible de modifier les regies dans le menu sortie

.. image:: img/tab_regie.png

En appuyant sur modification :

.. image:: img/form_regie.png


L'utilisateur doit saisir :

- libellé

les actions suivantes sont possibles

.. image:: img/action_regie.png

Solde facture en réglement régie
================================

Cette action permet de récupérer dans la régie les factures en réglement par régie.
Les factures sont incluses dans la régie et sont soldées.


Les factures payées lors de la regie
====================================

Il est possible de consulter les factures de la régie
dans le sous formulaire facture_regie.

.. image:: img/tab_facture_regie.png

Il est possible de sortir une facture de la régie avec l'action enlever la facture
de la régie" et la facture est en état soldée.

Edition de la régie
===================

L'édition reprend :

- le total général des factures en réglement de la régie

- le sous total par imputation budgétaire par prestation

- le sous total par imputation budgétaire pour les articles

- le sous total par catégorie de prestation

- le sous total par prestation

- le sous total par article

- le total par facture

.. image:: img/action_edition_regie.png

.. image:: img/action_edition_regie2.png


