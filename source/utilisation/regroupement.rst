.. _regroupement:

############
Regroupement
############

Le regroupement est utile si il n'y a pas de facturation. 

Le regroupement permet d'effectuer les sorties d'articles pour un regroupement qui peut être
un  service interne (exemple sortie de consommables informatiques),
une école (exemple caisse des écoles), un batîment (cas d'un atelier),
un véhicule (cas d'un garage)


Saisie des regroupements
========================

Il est possible de modifier les bons de commandes dans le menu sortie

.. image:: img/tab_regroupement.png

En appuyant sur modification :

.. image:: img/form_regroupement.png

Saisie des sorties articles
===========================

Il est possible de modifier les sorties articles dans l'onglet correspondant

.. image:: img/tab_sortie_article_regroupement.png

En appuyant sur + :

.. image:: img/form_sortie_article_regroupement.png


Attention cela impacte la sortie d'article dans le stock

Saisie des sorties prestations
==============================

Il est possible de modifier les sorties de prestation dans l'onglet correspondant


.. image:: img/tab_sortie_prestation_regroupement.png

En appuyant sur + :

.. image:: img/form_sortie_prestation_regroupement.png
